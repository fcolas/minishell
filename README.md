MiniShell
=========

Super simple C shell which uses `execvp`.

Compilation:

```
make
```

Execution:

```
./MiniShell
```

To exit just type:

```
exit
```

Here is a few extensions needed to be a real shell:

* Background exec: `&`
* Exec time command: `time`
* Redirection operators: `>` `<`...
* Tree directory navigation: `cd`
