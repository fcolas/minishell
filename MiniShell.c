#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>

#define ARG_MAX 200

void handle_signal (int sig);
int space_counter (char* str);
char** split (char* str);
void free_cmd (char** cmd);


/* Catch signal (e.g. c-C et c-D) */
void
handle_signal(int sig)
{
  printf ("\nMiniShell $ ");
  fflush (stdout);
}

/* Return the number of space in str */
int
space_counter(char* str)
{
  int i = 0, cnt = 0;

  if (str)
    {
      while (str[i] != '\0')
	{
	  if (str[i] == ' ')
	    cnt++;
	  i++;
	}
    }

  return cnt;
}

/* Split str at space in a 2-dim char array */
char**
split(char* str)
{
  int i = 0, j = 1;
  int space = space_counter (str);
  char** splt = ( char** ) malloc ((space+2) * sizeof(char*));
  
  splt[0] = str;
  while (str[i] != '\0')
    {
      if (str[i] == ' ')
	{
	  splt[j++] = str+i+1;
	  str[i] = '\0';
	}
      i++;
    }
  splt[j] = NULL;

  return splt;
}

/* Useful to free the 2-dim array of our cmd */
void
free_cmd(char** cmd)
{
  int i;

  for (i=0; cmd[i] != NULL; i++)
    free (cmd[i]);

  free (cmd[i+1]);
}

int
main()
{
  char* tmp = ( char* ) malloc (ARG_MAX * sizeof(char));
  char** cmd;
  char c = 0;
  int k = 0;

  signal (SIGINT, SIG_IGN);
  signal (SIGINT, handle_signal);

  printf ("MiniShell $ ");
  fflush (stdout);

  while ((c = getchar ()) != EOF)
    {
      switch (c)
	{
	case '\n': // 'Enter' keyboard input
	  // if we have a cmd let's execute it
	  if (!(tmp[0] == '\0') && !(tmp[0] == ' ') && !(tmp[0] == '\n'))
	    {
	      strncat (tmp, "\0", 1);

	      if (fork () == 0)
		{
		  cmd = split (tmp);

		  if (strcmp (cmd[0], "exit") == 0)
		    {
		      exit (1);
		    }
		  else
		    {
		      k = execvp (cmd[0], cmd);
		      if (k < 0)
			{
			  printf ("bash: %s: command not found \n", cmd[0]);
			  exit (1);
			}
		    }
		  free_cmd (cmd);

		}
	      else
		{
		  if (strncmp (tmp, "exit", 4) == 0)
		    exit (1);
		  else
		    wait (NULL);
		}
	    }
	  printf ("MiniShell $ ");
	  free (tmp);
	  tmp = ( char * ) malloc (ARG_MAX * sizeof(char));
	  bzero (tmp, ARG_MAX);
	  break;

	default: // Copy keyboard input char in tmp
	  strncat (tmp, &c, 1);
	  break;
	}
    }

  return 0;
}
