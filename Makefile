all: MiniShell

MiniShell: MiniShell.o
	gcc -o MiniShell MiniShell.o

MiniShell.o:
	gcc -c -Wall MiniShell.c

clean: 
	rm -f *.o
